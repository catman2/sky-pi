# Sky Pi

The short Java program allows for the audio transmission of images using several SSTV modes. See the project page[1] for more information. Compile and run dTV.java to load the program. Note that the version present here is essentially an SSTV demo rather than the complete telemetry program.

[1] https://gibbel.us/activities/sky_pi.html