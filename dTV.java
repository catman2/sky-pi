/*            '.
  .-""-._     \ \.--|
 /       "-..__) .-'
0_______0       /
 \'-.__,   .__.,'
  `'----'._\--'

Whale whale whale, what have we here?
Not any uncommented code, that's for sure.
It's cool though, this whole class is 
going bye-bye once everything works. */

/**
 * "Where are we going?" I asked
 * "I don't know," he said. "Just driving."
 * "But this road doesn't go anywhere," I told him.
 * "That doesn't matter."
 * "What does?" I asked, after a little while.
 * "Just that we're on it, dude," he said.
*/

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.File;
//import javax.comm.*;

public class dTV {
	public static void main(String[] args) {
		final int SAMPLE_RATE = 44100;
		boolean generateImage = true;
		boolean saveImage = false;
		int imageWidth = 320;
		int imageHeight = 256;
		System.out.println("Capturing image");
		BufferedImage image = new BufferedImage(imageWidth, imageHeight, 
												BufferedImage.TYPE_INT_RGB);
		if (generateImage) {
			for (int y = 0; y < imageHeight; y++) {
				for (int x = 0; x < imageWidth; x++) {
					image.setRGB(x, y, 0xaaffaa);
				}
			}
			if (saveImage == true) {
				try {
					File file = new File("imgTest.png");
					ImageIO.write(image, "png", file);
					System.out.println("Test image saved.");
				}
				catch (IOException e) {
					System.out.println("Error saving image");
				}
			}
		}
		else {
			try {
				image = ImageIO.read(new File("Stretched_Test_Pattern.png"));
			} 
			catch (IOException e) {
				System.out.println("Error reading image from file");
			}
		}

		System.out.println("Generating waveform buffer");
		SSTVWaveformGenerator genScotty1 = new SSTVWaveformGenerator(60, 
															SAMPLE_RATE, image);
		genScotty1.generateImage();
		SoundPlayer soundPlayer = new SoundPlayer(SAMPLE_RATE, 
												genScotty1.getAudioBuffer());
		try {
			System.out.println("Playing audio");
			soundPlayer.playSound();
		} 
		catch (javax.sound.sampled.LineUnavailableException e){
			System.out.println("Unable to open audio device");
		}


		System.out.println("Image sent");
	}

}